<?php

namespace AppBundle\Model;

use MyCLabs\Enum\Enum;

class Approval extends Enum
{
    const PENDING = 'pending';
    const APPROVED = 'approved';
}

