<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @Gedmo\Loggable
 */
class Post
{
    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';

    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\Column(nullable=true)
     * @Gedmo\Versioned
     */
    protected $status;

    /**
     * @ORM\Column(nullable=true)
     * @Gedmo\Versioned
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected $body;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->status = static::STATUS_APPROVED;
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }
}

