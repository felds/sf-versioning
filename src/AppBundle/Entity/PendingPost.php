<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class PendingPost extends Post
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Post::class)
     */
    private $post;

    public function __construct()
    {
        parent::__construct();

        $this->status = static::STATUS_PENDING;
    }

    static public function fromPost(Post $post)
    {
        $self = new static();

        $self->title = $post->title;
        $self->body = $post->body;
        $self->post = $post;

        return $self;
    }
}

