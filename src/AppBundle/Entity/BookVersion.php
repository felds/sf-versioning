<?php

namespace AppBundle\Entity;

use AppBundle\Model\Approval;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class BookVersion
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=Book::class, inversedBy="versions", cascade={"persist"})
     */
    protected $book;

    /**
     * @ORM\Column(nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(nullable=true)
     */
    protected $isbn;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(nullable=true)
     */
    protected $status;

    public function __construct(Book $book)
    {
        $this->id = Uuid::uuid4();
        $this->createdAt = new DateTimeImmutable();
        $this->status = Approval::PENDING();
        $this->book = $book;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    static public function from(self $last)
    {
        $self = new static($last->book);

        $self->title = $last->title;

        return $self;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setISBN($isbn)
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getISBN()
    {
        return $this->isbn;
    }

    public function approve()
    {
        $this->status = Approval::APPROVED();

        return $this;
    }
}

