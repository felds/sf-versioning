<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity=BookVersion::class, mappedBy="book")
     */
    protected $versions;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->versions = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->versions->last();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getVersions()
    {
        return $this->versions;
    }

    public function createVersion()
    {
        if ($this->versions->isEmpty()) {
            $version = new BookVersion($this);
        } else {
            $version = BookVersion::from($this->versions->last());
        }

        return $version;
    }
}

