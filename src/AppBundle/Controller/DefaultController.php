<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PendingPost;
use AppBundle\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request, EntityManagerInterface $em)
    {
        $entities = $em->getRepository(Post::class)->findBy([]);
        $pending = $em->getRepository(PendingPost::class)->findBy([]);

        return $this->render('Default/index.html.twig', [
            'entities' => $entities,
            'pending' => $pending,
        ]);
    }

    /**
     * @Route("/new")
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $entity = new Post();
        $form = $this->createPostForm($entity);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('Default/form.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    /**
     * @Route("/edit/{id}")
     */
    public function editAction(Post $entity, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createPostForm($entity);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $pending = PendingPost::fromPost($entity);

            $em->persist($pending);
            $em->refresh($entity);

            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('Default/form.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    /**
     * @Route("/approve/{id}")
     */
    public function approveAction(PendingPost $entity)
    {
        dump($entity);
    }

    private function createPostForm($entity)
    {
        $builder = $this->createFormBuilder($entity);

        $builder
            ->add('title')
            ->add('body')
        ;

        return $builder->getForm();
    }
}

