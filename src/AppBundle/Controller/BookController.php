<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\BookVersion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/book")
 */
class BookController extends Controller
{
    /**
     * @Route(name="books")
     */
    public function indexAction(EntityManagerInterface $em)
    {
        $entities = $em->getRepository(Book::class)->findBy([]);

        return $this->render('Book/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Route("/new", name="new_book")
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $book = new Book();
        $entity = $book->createVersion();

        $form = $this->createBookForm($entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($book);
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('books');
        }

        return $this->render('Book/form.html.twig', [
            'entity' => $book,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit_book")
     */
    public function editAction(Request $request, EntityManagerInterface $em, Book $book)
    {
        $entity = $book->createVersion();

        $form = $this->createBookForm($entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('books');
        }

        return $this->render('Book/form.html.twig', [
            'entity' => $book,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show_book")
     */
    public function showAction(Book $book)
    {
        return $this->render('Book/show.html.twig', [
            'entity' => $book,
        ]);
    }

    /**
     * @Route("/approve/{id}", name="approve_book")
     */
    public function approveAction(EntityManagerInterface $em, Book $book)
    {
        $version = $book->createVersion();
        $version->approve();

        $em->persist($version);
        $em->flush();

        return $this->redirectToRoute('books');
    }


    private function createBookForm(BookVersion $entity)
    {
        $builder = $this->createFormBuilder($entity);

        $builder
            ->add('title')
            ->add('isbn')
        ;

        return $builder->getForm();
    }
}

